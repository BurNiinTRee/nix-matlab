= Matlab for Nix users
:source-highlighter: rouge

This repo is a collection of files that should help Nix and NixOS users install Matlab imperatively.

== Install

Like any other Matlab user, you should first go to your Matlab account on
Mathworks' website, at: https://www.mathworks.com/mwaccount/ and download the
installation zip file. Extract the zip file and note the `install` executable
you just extracted. To run that file, you'll need to get into a FHS environment
that a nix-shell will provide.

=== Run `matlab-shell` with an unstable Nix

....
nix run gitlab:doronbehar/nix-matlab#matlab-shell
....

=== Run `matlab-shell` with +++<s>stable</s>+++ legacy Nix

....
git clone https://gitlab.com/doronbehar/nix-matlab
cd nix-matlab
nix-shell
....

=== Installation of Matlab itself

include::./install.adoc[]

=== Adding `matlab` executables to your system

With what was done now, you run Matlab from the command line with:

....
# Legacy nix users (inside a clone of this repo)
$(nix-build)/bin/matlab
# Flake nix users (without cloning)
nix run gitlab:doronbehar/nix-matlab
....

But it's likely you'd like to make Matlab survive garbage collections and add a
desktop launcher to your system. To do that you can follow one of the following
paths according to your setup.

==== For NixOS users with a flakes setup

[source,nix]
----
inputs = {
  # ...
  nix-matlab = {
    # Recommended if you also override the default nixpkgs flake, common among
    # nixos-unstable users:
    #inputs.nixpkgs.follows = "nixpkgs";
    url = "gitlab:doronbehar/nix-matlab";
  };
  # ...
  outputs = { self, nixpkgs, nix-matlab }:
  let
    flake-overlays = [
      nix-matlab.overlay
    ];
  in {
    nixosConfigurations = (
      HOSTNAME = nixpkgs.lib.nixosSystem {
        modules = [ (import ./configuration.nix flake-overlays) ]
      };
    };
  };
};
----

And in `./configuration.nix`:


[source,nix]
----

# All overlays given by flakes
flake-overlays:

{ config, pkgs, options, lib, ... }:
{
  nixpkgs.overlays = [
    (
      final: prev: {
        # Your own overlays...
      }
    )
  ] ++ flake-overlays;
}
----

Example evaluated
https://gitlab.com/doronbehar/nixos-configs/-/blob/e28b5aaf86f21202a2b7daa05b098ee5ed85de6f/flake.nix#L5-16[here]
and
https://gitlab.com/doronbehar/nixos-configs/-/blob/e28b5aaf86f21202a2b7daa05b098ee5ed85de6f/configuration.nix#L74[here].

==== For NixOS users without a flakes setup

The following setup should also fit flake NixOS users.

Add to your `configration.nix` (untested, but should work):

[source,nix]
----
  nixpkgs.overlays = let
    nix-matlab = import (builtins.fetchTarball "https://gitlab.com/doronbehar/nix-matlab/-/archive/master/nix-matlab-master.tar.gz");
  in [
    nix-matlab.overlay
    (
      final: prev: {
        # Your own overlays...
      }
    )
  ];
----

==== Home Manager setup

TODO

==== Usage in Other Flakes / `shell.nix`

Some people may wish to not install matlab globally, and only making it part of
the `buildInputs` of their project. Usually this paradigm follows along with
https://direnv.net/[`direnv`]
+ https://nixos.wiki/wiki/Development_environment_with_nix-shell#Using_Direnv[`shell.nix`]
/ https://nixos.wiki/wiki/Flakes#Direnv_integration[`flake.nix`] setup. For
example you can create in your project a `shell.nix`, or define `devShell` in
your `flake.nix` similarly to this: 

[source,nix]
----
{ pkgs, nix-matlab }:

pkgs.mkShell {
  buildInputs = (with nix-matlab.packages.x86_64-linux; [
    matlab
    matlab-mlint
    matlab-mex
  ]);
  # Define C include path env vars for c development and PYTHONPATH for `import matlab`.
  shellHook = nix-matlab.shellHooksCommon;
}
----

Note that Matlab still needs to be installed in a user-writeable location for
this `shellHook` to work, as explained xref:#user-content-install[here].

== FAQ

=== Matlab says that my license was installed for a different Host ID, why does it happen?

I haven't had reports of this issue by users of this repo, but I experience it
myself sometimes, and I think it's a good thing to include in this README. I
use Gnome with NetworkManager on my laptop, and rarely I use USB tethering with
my Android. From some reason while I'm tethered, I get the following error when
I launch matlab from the command line:

----
The hostid of your computer ("000000000000 111111111111") does not match the hostid of the license
file (XXXXXXXXXXXX).
----

I get this error although I set in my `/etc/nixos/configuration.nix`:

[source,nix]
----
networking.hostId = "6b216943" # fake id
----

As explained in the https://www.mathworks.com/support/lme/R2021a/9[link in the
full error message], I can reactivate the license and not go again through the
whole installation procedure. But I will be required to reactivate the license
again afterwards when I switch again to WiFi internet connection. It's somewhat
annoying, and I haven't found a solution / explanation why does my host id
changes when I switch to a wired connection.

== Previous work / Credits

Core parts of this repo are based on
https://github.com/tviti/nix-cfg/tree/e6531426e86273e450c6133a0deae9008411fffd/pkgs/matlab[@tviti's
work]. My part was making it a bit more accessible for modern Nix flake setup,
and making the shell and steps a bit more approachable.

The idea for a `shellHooksCommon` was by
https://gitlab.com/DavidRConnell[@DavidRConnell], first discussed in
https://gitlab.com/doronbehar/nix-matlab/-/merge_requests/1#note_631741222[!1].
